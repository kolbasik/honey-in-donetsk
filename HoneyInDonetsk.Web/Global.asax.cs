﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;

namespace HoneyInDonetsk.Web
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            RegisterRoutes(RouteTable.Routes);
            RegisterFilters(GlobalFilters.Filters);
            RegisterViewEngines(ViewEngines.Engines);
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.LowercaseUrls = true;
            routes.AppendTrailingSlash = true;

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }

        public static void RegisterFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterViewEngines(ViewEngineCollection viewEngines)
        {
            viewEngines.Where(x => x is RazorViewEngine == false).ToList().ForEach(engine => viewEngines.Remove(engine));
        }
    }
}