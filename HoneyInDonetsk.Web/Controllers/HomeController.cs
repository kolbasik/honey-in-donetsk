﻿using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using HoneyInDonetsk.Web.Core;

namespace HoneyInDonetsk.Web.Controllers
{
    public class HomeController : Controller
    {
        [OutputCache(CacheProfile = "HomePage")]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost, ActionName("Index"), AsyncTimeout(1000)]
        public async Task<ActionResult> SendEmail([ModelBinder(typeof(EmailDtoBinder))] EmailDto dto)
        {
            if (IsValid(this.Request))
            {
                const string subject = "[Заказ][Мёд]";
                string message = GetEmailBody(dto);
                await EmailSender.Send(dto.Email, dto.Name, subject, message);
            }
            return RedirectToAction("Index");
        }

        private bool IsValid(HttpRequestBase request)
        {
            return BotDetector.IsValidHoneySpot(request);
        }

        private string GetEmailBody(EmailDto dto)
        {
            return ViewRenderer.Render(ControllerContext, "Email", dto);
        }
    }
}