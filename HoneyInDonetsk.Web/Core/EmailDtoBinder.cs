﻿using System.Web.Mvc;

namespace HoneyInDonetsk.Web.Core
{
    public class EmailDtoBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var form = controllerContext.HttpContext.Request.Form;
            var dto = new EmailDto
                {
                    Name = form["name"] ?? "",
                    Email = form["email"] ?? "",
                    Subject = form["subject"] ?? "",
                    Message = form["message"] ?? ""
                };
            return dto;
        }
    }
}