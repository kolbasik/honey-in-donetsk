﻿using System.Web;

namespace HoneyInDonetsk.Web.Core
{
    public static class BotDetector
    {
        public static bool IsValidHoneySpot(HttpRequestBase request)
        {
            return string.IsNullOrWhiteSpace(request.Form["honeyspot"]);
        }
    }
}