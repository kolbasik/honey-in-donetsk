﻿using System;
using System.Diagnostics;
using System.Net.Mail;
using System.Threading.Tasks;

namespace HoneyInDonetsk.Web.Core
{
    public static class EmailSender
    {
        public static async Task<bool> Send(string email, string name, string subject, string message)
        {
            bool state = false;
            try
            {
                var address = new MailAddress(email, name);
                using (var mail = new MailMessage { From = address })
                {
                    mail.To.Add("honey.in.donetsk@gmail.com");

                    mail.Subject = subject;
                    mail.IsBodyHtml = true;
                    mail.Body = message;

                    using (var smtp = new SmtpClient())
                        await smtp.SendMailAsync(mail);

                    state = true;
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
            }
            return state;
        }
    }
}